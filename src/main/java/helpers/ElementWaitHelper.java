package helpers;

import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.FluentWait;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import tools.driverFactory.DriverManager;

import java.time.Duration;

import static org.openqa.selenium.support.ui.ExpectedConditions.elementToBeClickable;
import static org.openqa.selenium.support.ui.ExpectedConditions.visibilityOf;

public class ElementWaitHelper {
    private static final Logger LOGGER = LoggerFactory.getLogger(ElementWaitHelper.class);
    private static final Duration DEFAULT_TIMEOUT_VALUE = Duration.ofSeconds(120);
    private static final Duration DEFAULT_POLLING_INTERVAL = Duration.ofMillis(250);

    private ElementWaitHelper() {
        // Private constructor to prevent instantiation (singleton)
    }

    public static void waitForElementToBeVisible(WebElement elemnt) {
        waitForElementToBeVisible(elemnt, DEFAULT_TIMEOUT_VALUE);
    }

    private static void waitForElementToBeVisible(WebElement element, Duration timeoutDuration) {
        LOGGER.info("Waiting for element to be visible...");
        condition(visibilityOf(element), timeoutDuration);
    }

    public static void waitForElementToBeClickable(WebElement element) {
        waitForElementToBeClickable(element, DEFAULT_TIMEOUT_VALUE);
    }

    private static void waitForElementToBeClickable(WebElement element, Duration timeoutDuration) {
        LOGGER.info("Waiting for element to be clickable...");
        condition(elementToBeClickable(element), timeoutDuration);
    }

    @SuppressWarnings("unchecked")
    private static void condition(ExpectedCondition<?> condition, Duration timeoutDuration) {
        FluentWait wait = new FluentWait(DriverManager.getDriver())
                .withTimeout(timeoutDuration)
                .pollingEvery(DEFAULT_POLLING_INTERVAL)
                .ignoring(NoSuchElementException.class);
        wait.until(condition);
    }
}
