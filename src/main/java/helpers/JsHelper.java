package helpers;

import org.openqa.selenium.JavascriptExecutor;
import tools.driverFactory.DriverManager;

public class JsHelper {
    public void scrollToBottom() {
        try {
            long lastHeight = (long) ((JavascriptExecutor) DriverManager.getDriver()).executeScript("return document.body.scrollHeight");

            while (true) {
                ((JavascriptExecutor) DriverManager.getDriver()).executeScript("window.scrollTo(0, document.body.scrollHeight);");
                Thread.sleep(2000);

                long newHeight = (long) ((JavascriptExecutor) DriverManager.getDriver()).executeScript("return document.body.scrollHeight");
                if (newHeight == lastHeight) {
                    break;
                }
                lastHeight = newHeight;
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
