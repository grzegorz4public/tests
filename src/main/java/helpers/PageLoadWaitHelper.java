package helpers;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static tools.driverFactory.DriverManager.getDriver;
import static tools.driverFactory.DriverManager.getWait;

public class PageLoadWaitHelper {
    private static final Logger LOGGER = LoggerFactory.getLogger(PageLoadWaitHelper.class);

    private PageLoadWaitHelper() {
        // Private constructor to prevent instantiation (singleton)
    }

    public static void waitForPageToLoad() {
        LOGGER.info("Wait for page to load...");
        getWait(getDriver()).until((ExpectedCondition<Boolean>) wd ->
                ((JavascriptExecutor) wd).executeScript("return document.readyState").equals("complete"));
    }
}
