package assertions;

import org.assertj.core.api.AbstractAssert;
import org.assertj.core.api.SoftAssertions;
import pages.GoogleSearchPage;

public class GoogleSearchPageAssert extends AbstractAssert<GoogleSearchPageAssert, GoogleSearchPage> {

    public GoogleSearchPageAssert(GoogleSearchPage actual) {
        super(actual, GoogleSearchPageAssert.class);
    }

    public GoogleSearchPageAssert hasAllSearchFormElementEnabled() {
        SoftAssertions softly = new SoftAssertions();
        softly.assertThat(actual.getSearchInputElement().isEnabled())
                .describedAs("Search Input is enabled")
                .isTrue();
        softly.assertThat(actual.getSearchButtonElement().isEnabled())
                .describedAs("Search Google button is enabled")
                .isTrue();
        softly.assertAll();

        return this;
    }
}
