package assertions;

import org.assertj.core.api.AbstractAssert;
import org.assertj.core.api.SoftAssertions;
import pages.GoogleSearchResultPage;

public class GoogleSearchResultsPageAssert extends AbstractAssert<GoogleSearchResultsPageAssert, GoogleSearchResultPage> {

    public GoogleSearchResultsPageAssert(GoogleSearchResultPage actual) {
        super(actual, GoogleSearchResultsPageAssert.class);
    }

    public GoogleSearchResultsPageAssert hasNonEmptySearchResults() {
        if(actual.getSearchResultsRows().isEmpty())
            failWithMessage("Search Results are empty");

        return this;
    }

    public GoogleSearchResultsPageAssert hasExpectedResultWithTextOnPage(String expectedResult) {
        if(actual.getSearchResultsWithText(expectedResult).isEmpty())
            failWithMessage("Search Results Page doesn't contain expected text in result");

        return this;
    }

    public GoogleSearchResultsPageAssert hasExpectedResultWithUrlOnPage(String expectedResult) {
        if(actual.getSearchResultsWithUrl(expectedResult).isEmpty())
            failWithMessage("Search Results Page doesn't contain expected url in results");

        return this;
    }

    public GoogleSearchResultsPageAssert hasEmbeddedSearchElementsFor(String expectedResult) {
        SoftAssertions softly = new SoftAssertions();
        softly.assertThat(actual.getEmbeddedSearchInput().isDisplayed())
                .describedAs("Embedded Search Input is displayed")
                .isTrue();
        softly.assertThat(actual.getEmbeddedSearchButton().isDisplayed())
                .describedAs("Embedded Search Button is displayed")
                .isTrue();
        softly.assertAll();

        return this;
    }

    public GoogleSearchResultsPageAssert hasExpectedQueryInInput(String expectedQuery) {
        if(!actual.getSearchInput().getAttribute("value").equals(expectedQuery))
            failWithMessage("Query is not as expected");

        return this;
    }
}
