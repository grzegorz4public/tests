package assertions;

import org.assertj.core.api.AbstractAssert;
import org.assertj.core.api.SoftAssertions;
import pages.ScrumOrgCertificationListPage;

public class ScrumOrgCertificationListPageAssert extends AbstractAssert<ScrumOrgCertificationListPageAssert, ScrumOrgCertificationListPage> {

    public ScrumOrgCertificationListPageAssert(ScrumOrgCertificationListPage actual) {
        super(actual, ScrumOrgCertificationListPageAssert.class);
    }

    public ScrumOrgCertificationListPageAssert hasAllExpectedInputElements() {
        SoftAssertions softly = new SoftAssertions();
        softly.assertThat(actual.getLastNameElement().isDisplayed())
                .describedAs("Last Name Input element is displayed")
                .isTrue();
        softly.assertThat(actual.getSearchButtonElement().isDisplayed())
                .describedAs("Search Button is displayed")
                .isTrue();
        softly.assertAll();

        return this;
    }

    public ScrumOrgCertificationListPageAssert hasReturnedSearchResults() {
        if (!actual.getSearchResultInnerElement().isDisplayed())
            failWithMessage("Search Results are not displayed");

        return this;
    }
}
