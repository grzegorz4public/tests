package assertions;

import org.assertj.core.api.Assertions;
import pages.ScrumOrgCertificationListPage;
import pages.GoogleSearchPage;
import pages.GoogleSearchResultPage;

public class PageAssertions extends Assertions {

    public static GoogleSearchPageAssert assertThat(GoogleSearchPage actual) {
        return new GoogleSearchPageAssert(actual);
    }

    public static GoogleSearchResultsPageAssert assertThat(GoogleSearchResultPage actual) {
        return new GoogleSearchResultsPageAssert(actual);
    }

    public static ScrumOrgCertificationListPageAssert assertThat(ScrumOrgCertificationListPage actual) {
        return new ScrumOrgCertificationListPageAssert(actual);
    }

}
