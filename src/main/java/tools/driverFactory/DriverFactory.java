package tools.driverFactory;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.ie.InternetExplorerOptions;

import static constants.Browsers.*;

class DriverFactory {
    private static final DriverOptionsManager DRIVER_OPTIONS_MANAGER = new DriverOptionsManager();

    static synchronized WebDriver createDriver(String browser) throws IllegalArgumentException {
        WebDriver driver;
        switch (browser) {
            case CHROME:
                ChromeOptions chromeOptions = DRIVER_OPTIONS_MANAGER.getChromeOptions();
                driver = new ChromeDriver(chromeOptions);
                break;
            case FIREFOX:
                FirefoxOptions firefoxOptions = DRIVER_OPTIONS_MANAGER.getFirefoxOptions();
                driver = new FirefoxDriver(firefoxOptions);
                break;
            case IE:
                InternetExplorerOptions internetExplorerOptions = DRIVER_OPTIONS_MANAGER.getInternetExplorerOptions();
                driver = new InternetExplorerDriver(internetExplorerOptions);
                break;
            default:
                throw new IllegalArgumentException("Unrecognized browser selected!");
        }
        driver.manage().window().maximize();
        return driver;
    }
}