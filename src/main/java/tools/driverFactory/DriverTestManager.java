package tools.driverFactory;

import org.openqa.selenium.WebDriver;

public class DriverTestManager {

    public static void setDriver(String browser) {
            WebDriver webDriver = DriverFactory.createDriver(browser);
            DriverManager.setWebDriver(webDriver);
    }

    public static void tearDown() {
        WebDriver webDriver = DriverManager.getDriver();
        if (webDriver != null) {
            webDriver.manage().deleteAllCookies();
            webDriver.quit();
        }
    }
}
