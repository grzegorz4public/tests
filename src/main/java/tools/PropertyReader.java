package tools;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

import static constants.Browsers.*;

public class PropertyReader {

    public String getBrowser() {
        return loadPropertyFile().getProperty("browser");
    }

    public void setDriverPath(String browser) {
        switch (browser) {
            case CHROME:
                System.setProperty("webdriver.chrome.driver", loadPropertyFile().getProperty("chromeDriverPath"));
                break;
            case FIREFOX:
                System.setProperty("webdriver.gecko.driver", loadPropertyFile().getProperty("firefoxDriverPath"));
                break;
            case IE:
                System.setProperty("webdriver.ie.driver", loadPropertyFile().getProperty("ieDriverPath"));
                break;
        }
    }

    private Properties loadPropertyFile() {
        Properties propertyFile = new Properties();
        try {
            propertyFile.load(new FileInputStream("src/main/resources/config.properties"));
        } catch (IOException e) {
            e.printStackTrace();
        }

        return propertyFile;
    }
}
