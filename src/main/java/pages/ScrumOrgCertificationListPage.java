package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import static helpers.ElementWaitHelper.waitForElementToBeVisible;
import static helpers.PageLoadWaitHelper.waitForPageToLoad;

public class ScrumOrgCertificationListPage extends AbstractPageObject {
    private static final String SCRUM_ORG_CERTIFICATE_SEARCH_URL = "https://www.scrum.org/certification-list";

    private static final By SEARCH_RESULT_ROW_LOCATOR = By.className("responsive-table-row");
    private static final By SEARCH_RESULTS_CELL_LOCATOR = By.className("responsive-table-cell");

    private static final int HEADING_NAME_INDEX = 0;
    private static final int HEADING_DATE_INDEX = 3;

    @FindBy(id = "edit-last-name")
    WebElement editLastNameInputElement;

    @FindBy(id = "edit-search")
    WebElement editSearchButtonElement;

    @FindBy(className = "site-footer")
    WebElement footerElement;

    @FindBy(className = "search-results-inner")
    WebElement searchResultInnerElement;

    public WebElement getLastNameElement() {
        return editLastNameInputElement;
    }

    public WebElement getSearchButtonElement() {
        return editSearchButtonElement;
    }

    public WebElement getSearchResultInnerElement() {
        return searchResultInnerElement;
    }

    private List<WebElement> getResultRows() {
        return driver.findElements(SEARCH_RESULT_ROW_LOCATOR);
    }

    private List<WebElement> getResultsForMember(String firstName, String lastName) {
        return getResultRows().stream()
                .filter(resultRow -> resultRow.findElements(SEARCH_RESULTS_CELL_LOCATOR)
                        .get(HEADING_NAME_INDEX).getText().equals(firstName + " " + lastName))
                .collect(Collectors.toList());
    }

    public LocalDate getOldestCertificateForMember(String firstName, String lastName) {
        List<WebElement> resultsForMember = getResultsForMember(firstName, lastName);

        return getSortedDatesFromCells(resultsForMember).stream().min(LocalDate::compareTo).get();
    }

    private List<LocalDate> getSortedDatesFromCells(List<WebElement> resultRows) {
        List<LocalDate> certificationDates = new ArrayList<>();
        DateTimeFormatter df = DateTimeFormatter.ofPattern("M.d.yy");

        for (WebElement resultRow : resultRows) {
            certificationDates.add(LocalDate.parse(resultRow.findElements(SEARCH_RESULTS_CELL_LOCATOR).get(HEADING_DATE_INDEX).getText(), df));
        }

        return certificationDates;
    }

    public ScrumOrgCertificationListPage open() {
        open(SCRUM_ORG_CERTIFICATE_SEARCH_URL);
        waitForPageToLoad();

        return this;
    }

    public ScrumOrgCertificationListPage fillLastName(String lastName) {
        editLastNameInputElement.sendKeys(lastName);

        return this;
    }

    public ScrumOrgCertificationListPage performSearch() {
        editSearchButtonElement.submit();
        waitForElementToBeVisible(searchResultInnerElement);

        return this;
    }



}
