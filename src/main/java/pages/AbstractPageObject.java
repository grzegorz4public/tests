package pages;

import helpers.JsHelper;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import tools.driverFactory.DriverManager;

public class AbstractPageObject {

    final WebDriver driver = DriverManager.getDriver();

    AbstractPageObject() {
        PageFactory.initElements(DriverManager.getDriver(), this);
    }

    void open(String url) {
        driver.get(url);
    }

    public AbstractPageObject scrollToBottom() {
        new JsHelper().scrollToBottom();

        return this;
    }

}
