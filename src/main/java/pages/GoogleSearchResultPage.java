package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.List;
import java.util.stream.Collectors;

import static helpers.PageLoadWaitHelper.waitForPageToLoad;

public class GoogleSearchResultPage extends AbstractPageObject {

    private static final String HTML_TAG_NAME_H3 = "h3";
    private static final String HTML_TAG_NAME_A = "a";
    private static final String HTML_ATTRIBUTE_HREF = "href";

    @FindBy(name = "q")
    WebElement searchInput;

    @FindBy(id = "search")
    WebElement searchResults;

    @FindBy(className = "bkWMgd")
    List<WebElement> searchResultRows;

    @FindBy(id = "nqsbq")
    WebElement embeddedSearchInput;

    @FindBy(name = "btnGNS")
    WebElement embeddedSearchButton;

    WebElement getSearchResults() {
        return searchResults;
    }

    public WebElement getEmbeddedSearchInput() {
        return embeddedSearchInput;
    }

    public WebElement getEmbeddedSearchButton() {
        return embeddedSearchButton;
    }

    public List<WebElement> getSearchResultsRows() {
        return searchResultRows;
    }

    public List<WebElement> getSearchResultsWithText(String text) {
        return getSearchResultsRows()
                .stream()
                .filter(webElement ->
                        webElement.findElement(By.tagName(HTML_TAG_NAME_H3))
                                .getText().equals(text))
                .collect(Collectors.toList());
    }

    public List<WebElement> getSearchResultsWithUrl(String url) {
        return getSearchResultsRows()
                .stream()
                .filter(webElement ->
                        webElement.findElement(By.tagName(HTML_TAG_NAME_A))
                                .getAttribute(HTML_ATTRIBUTE_HREF).equals(url))
                .collect(Collectors.toList());
    }

    public WebElement getSearchInput() {
        return searchInput;
    }

    public GoogleSearchResultPage embeddedSearchFor(String searchQuery) {
        embeddedSearchInput.sendKeys(searchQuery);
        embeddedSearchButton.click();
        waitForPageToLoad();

        return this;
    }

}
