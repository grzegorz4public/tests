package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import static helpers.ElementWaitHelper.waitForElementToBeVisible;
import static helpers.PageLoadWaitHelper.waitForPageToLoad;

public class GoogleSearchPage extends AbstractPageObject {
    private static final String GOOGLE_SEARCH_PAGE_URL = "https://www.google.com/ncr";

    @FindBy(name = "q")
    WebElement searchInput;

    @FindBy(name = "btnK")
    WebElement searchButton;

    public WebElement getSearchInputElement() {
        return searchInput;
    }

    public WebElement getSearchButtonElement() {
        return searchButton;
    }

    public GoogleSearchPage open() {
        open(GOOGLE_SEARCH_PAGE_URL);
        waitForPageToLoad();

        return this;
    }

    public GoogleSearchPage searchFor(String queryString) {
        searchInput.sendKeys(queryString);

        return this;
    }

    public GoogleSearchResultPage clickSearchGoogle() {
        searchButton.submit();
        waitForPageToLoad();
        GoogleSearchResultPage googleSearchResultPage = new GoogleSearchResultPage();
        waitForElementToBeVisible(googleSearchResultPage.getSearchResults());

        return googleSearchResultPage;
    }

}
