import assertions.PageAssertions;
import org.testng.annotations.Test;
import pages.GoogleSearchPage;
import pages.GoogleSearchResultPage;

public class GoogleSearchTest extends BaseTest {
    private static final String MAIN_SEARCH_QUERY = "microsoft";
    private static final String EXPECTED_SEARCH_RESULT = "Microsoft - Official Home Page";
    private static final String EMBEDDED_SEARCH_QUERY = "surface";
    private static final String EXPECTED_QUERY = "surface site:microsoft.com";
    private static final String EXPECTED_URL_IN_SEARCH_RESULT = "https://www.microsoft.com/en-us/surface";

    @Test
    public void searchResultTest() {
        GoogleSearchPage googleSearchPage = new GoogleSearchPage().open();
        PageAssertions.assertThat(googleSearchPage)
                .hasAllSearchFormElementEnabled();

        GoogleSearchResultPage googleSearchResultPage = googleSearchPage
                .searchFor(MAIN_SEARCH_QUERY)
                .clickSearchGoogle();
        PageAssertions.assertThat(googleSearchResultPage)
                .hasNonEmptySearchResults()
                .hasExpectedResultWithTextOnPage(EXPECTED_SEARCH_RESULT)
                .hasEmbeddedSearchElementsFor(EXPECTED_SEARCH_RESULT);

        googleSearchResultPage.embeddedSearchFor(EMBEDDED_SEARCH_QUERY);
        PageAssertions.assertThat(googleSearchResultPage)
                .hasExpectedQueryInInput(EXPECTED_QUERY)
                .hasExpectedResultWithUrlOnPage(EXPECTED_URL_IN_SEARCH_RESULT);
    }
}
