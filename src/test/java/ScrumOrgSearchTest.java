import assertions.PageAssertions;
import org.assertj.core.api.Assertions;
import org.testng.annotations.Test;
import pages.ScrumOrgCertificationListPage;

import java.time.LocalDate;

public class ScrumOrgSearchTest extends BaseTest {
    private static final String FIRST_NAME = "Andy";
    private static final String LAST_NAME = "Brandt";
    private static final String CERTIFICATION_DATE = "2010-06-21";


    @Test
    public void scrumOrgCertificateSearch() {
        ScrumOrgCertificationListPage scrumOrgSearchPage = new ScrumOrgCertificationListPage().open();
        PageAssertions.assertThat(scrumOrgSearchPage)
                .hasAllExpectedInputElements();

        scrumOrgSearchPage
                .fillLastName(LAST_NAME)
                .performSearch();
        PageAssertions.assertThat(scrumOrgSearchPage)
                .hasAllExpectedInputElements()
                .hasReturnedSearchResults();
        scrumOrgSearchPage.scrollToBottom();

        LocalDate oldestCertificateForMember = scrumOrgSearchPage.getOldestCertificateForMember(FIRST_NAME, LAST_NAME);

        Assertions.assertThat(oldestCertificateForMember)
                .describedAs("Oldest certification date should be the same as expected")
                .isEqualTo(CERTIFICATION_DATE);
    }
}
