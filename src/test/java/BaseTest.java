import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import tools.PropertyReader;
import tools.driverFactory.DriverTestManager;

public class BaseTest {
    private static final Logger LOGGER = LoggerFactory.getLogger(BaseTest.class);

    @BeforeMethod
    @Parameters({"browser"})
    public void prepareDriver(@Optional String browser) {
        LOGGER.info("Preparing driver...");
        PropertyReader browserProperty = new PropertyReader();
        if(null == browser) {
            LOGGER.info("Browser not specified as parameter, reading from config...");
            browser = browserProperty.getBrowser();
        }
        LOGGER.info("Setting driver for: " + browser);
        browserProperty.setDriverPath(browser);
        DriverTestManager.setDriver(browser);
    }

    @AfterMethod
    public void cleanAfterTest() {
        LOGGER.info("Tear down after test...");
        DriverTestManager.tearDown();
    }
}
